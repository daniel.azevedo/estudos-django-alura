from django.contrib import admin
from.models import Receita

class ListandoReceitas(admin.ModelAdmin):
    list_display = ('id', 'publicada', 'nome_receita', 'categoria', 'tempo_preparo')
    list_display_links =  ('id', 'nome_receita', 'categoria', 'tempo_preparo')
    search_fields = ('nome_receita',)
    list_filter = ('categoria',)
    list_editable = ('publicada',)
    list_per_page = 10


admin.site.register(Receita, ListandoReceitas)
