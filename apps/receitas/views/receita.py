import imp
from django.shortcuts import render, redirect, get_list_or_404, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from django.contrib.auth.models import User


from receitas.models import Receita


def index(request):
    receitas = Receita.objects.order_by('-date_receita').filter(publicada=True)
    paginator = Paginator(receitas, 3)
    page = request.GET.get('page', 1)
    receitas_por_pagina = paginator.get_page(page)
    dados = {
        'receitas': receitas_por_pagina
    }
    return render(request, 'receitas/index.html', context=dados)

def receita(request, receita_id):
    receita = get_object_or_404(Receita, pk=receita_id)
    receita_a_exibir = {
        'receita': receita
    }
    return render(request, 'receitas/receita.html', context=receita_a_exibir)

def cria_receita(request):
    if request.method == 'POST':
        nome_receita = request.POST['nome_receita']
        ingredientes = request.POST['ingredientes']
        modo_preparo = request.POST['modo_preparo']
        tempo_preparo = request.POST['tempo_preparo']
        rendimento = request.POST['rendimento']
        categoria = request.POST['categoria']
        foto_receita = request.FILES['foto_receita']
        receita_publicada = request.POST.get('publicada', False)
        user = get_object_or_404(User, pk=request.user.id)
        receita = Receita.objects.create(
            pessoa=user,
            nome_receita=nome_receita,
            ingredientes=ingredientes,
            modo_preparo=modo_preparo,
            tempo_preparo=tempo_preparo,
            rendimento=rendimento,
            categoria=categoria,
            foto_receita=foto_receita,
            publicada=receita_publicada
        )
        receita.save()
        print('receita criada com sucesso')
        return redirect('usuarios/dashboard')
    else:
        return render(request, 'receitas/criar_receita.html')

def deleta_receita(request, receita_id):
    receita = get_object_or_404(Receita, pk=receita_id)
    receita.delete()
    return redirect('dashboard')

def edita_receita(request, receita_id):
    receita = get_object_or_404(Receita, pk=receita_id)
    
    return render(request, 'receitas/edita_receita.html', context={'receita': receita})

def atualiza_receita(request):
    if request.method == "POST":
        receita_id = request.POST['receita_id']
        receita_a_editar = Receita.objects.get(pk=receita_id)
        receita_a_editar.nome_receita = request.POST['nome_receita']
        receita_a_editar.ingredientes = request.POST['ingredientes']
        receita_a_editar.modo_preparo = request.POST['modo_preparo']
        receita_a_editar.tempo_preparo = request.POST['tempo_preparo']
        receita_a_editar.rendimento = request.POST['rendimento']
        receita_a_editar.categoria = request.POST['categoria']
        receita_a_editar.publicada = request.POST.get('publicada', False)
        if 'foto_receita' in request.FILES:
            receita_a_editar.foto_receita = request.FILES['foto_receita']
        receita_a_editar.save()
        messages.info(request, 'Receita editada com sucesso!')
        return redirect('receita', receita_id=receita_id)


