from django.urls import path
from .views.receita import *
from .views.busca import *
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', index, name='index'),
    path('<int:receita_id>', receita, name='receita'),
    path('buscar', busca, name='buscar'),
    path('criar_receita', cria_receita, name='criar_receita'),
    path('deleta/<int:receita_id>', deleta_receita, name='deleta_receita'),
    path('edita/<int:receita_id>', edita_receita, name='edita_receita'),
    path('atualiza_receita', atualiza_receita, name='atualiza_receita'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
