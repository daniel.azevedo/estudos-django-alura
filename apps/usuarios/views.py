from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.models import User
from django.contrib import auth, messages
from receitas.models import Receita


def cadastro(request):
    """Cadastra uma nova pessoa no sistema"""
    if request.method == 'POST':
        nome = request.POST['nome']
        email = request.POST['email']
        senha1 = request.POST['password']
        senha2 = request.POST['password2']
        if campo_vazio(nome):
            messages.error(request, 'O campo nome não pode ficar em branco')
            return redirect('cadastro')
        if campo_vazio(email):
            messages.error(request, 'O campo email não pode ficar em branco')
            return redirect('cadastro')
        if senhas_nao_sao_iguais(senha1, senha2):
            messages.error(request, 'As senhas não são iguais')
            return redirect('cadastro')
        if usuario_existe(nome, email):
            messages.error(request, 'Usuário já cadastrado')
            return redirect('cadastro')
        
        usuario = User.objects.create_user(username=nome, email=email, password=senha1)
        usuario.save()
        messages.success(request, 'Usuário cadastrado com sucesso')
        return redirect('login')
    elif request.user.is_authenticated:
        return render(request, 'usuarios/dashboard.html')
    else:
        return render(request, 'usuarios/cadastro.html')


def login(request):
    """Realiza o login de uma pessoa no sistema"""
    if request.method == 'POST':
        email = request.POST['email']
        senha = request.POST['senha']
        if email == "" or senha == "":
            print('Os campos email e senha não podem fixar em branco')
            return redirect('login')
        print(email, senha)
        if User.objects.filter(email=email).exists():
            nome = User.objects.filter(email=email).values_list('username', flat=True).get()
            user = auth.authenticate(request, username=nome, password=senha)
            if user is not None:
                auth.login(request, user)
                print('Login realizado com sucesso')
                return redirect('dashboard')
            else:
                return redirect('login')
    return render(request, 'usuarios/login.html')

def logout(request):
    auth.logout(request)
    messages.info(request, 'Usuário deslogado')
    return redirect('login')

def dashboard(request):
    if request.user.is_authenticated:
        id = request.user.id
        receitas = Receita.objects.filter(pessoa=id).order_by('-date_receita')
        dados= {'receitas': receitas}
        return render(request, 'usuarios/dashboard.html', context=dados)
    else:
        return redirect('index')

def campo_vazio(campo):
    return bool(not len(campo.strip()))

def senhas_nao_sao_iguais(senha1, senha2):
    return senha1 != senha2

def usuario_existe(nome, email):
    return (User.objects.filter(email=email).exists()
            or User.objects.filter(username=nome).exists())
